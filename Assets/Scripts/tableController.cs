﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class tableController : MonoBehaviour {

    public GameObject Data;
    public Text Angle;
    int No;
    [SerializeField] Button graphButton;
    [SerializeField] string hdrCSV, fNameCSV, strResPathObs;
    [SerializeField] Text result, log;

	// Use this for initialization
	void Start () {
        if(string.IsNullOrEmpty(hdrCSV)){
            hdrCSV = "session_id,write_time,obs_no,theta,s,t1,t2,T,2S,t^2,A,G";
        }
        if(string.IsNullOrEmpty(fNameCSV)){
            fNameCSV = "InclinedPlaneObs.txt";
        }
    }
	
	// Update is called once per frame
	void Update () {
        // graphButton.interactable = mayShowGraph();
    }
    void OnEnable()
    {
        if (PlayerPrefs.HasKey("FinalAngle"))
        {
            Angle.text = "θ = "+ PlayerPrefs.GetString("FinalAngle").ToString()+ "°";
        }
        else
        {
            Angle.text = "θ = " + PlayerPrefs.GetString("Angle").ToString()+ "°";
        }

        while (this.transform.childCount > 0)
        {
            Transform c = this.transform.GetChild(0);
            c.SetParent(null);
            Destroy(c.gameObject);
        }


        for (int i = 0; i < PlayerPrefs.GetInt("No"); i++)
        {
            No = i + 1;
            GameObject go = Instantiate(Data);
            go.name = "DR_" + i;
            go.transform.SetParent(this.transform, false);
            go.transform.Find("No").GetComponent<Text>().text = No.ToString();
            go.transform.Find("S").GetComponent<Text>().text = PlayerPrefs.GetString("S[" + No + "]");
            go.transform.Find("T1").GetComponent<Text>().text = PlayerPrefs.GetString("T1[" + No + "]");
            if (PlayerPrefs.HasKey("T2[" + No + "]"))
            {
                go.transform.Find("T2").GetComponent<Text>().text = PlayerPrefs.GetString("T2[" + No + "]");
            }
            if (PlayerPrefs.HasKey("T[" + No + "]"))
            {
                go.transform.Find("T").GetComponent<InputField>().text =PlayerPrefs.GetString("T[" + No + "]");
            }
            if (PlayerPrefs.HasKey("2S[" + No + "]"))
            {
                go.transform.Find("2S").GetComponent<InputField>().text = PlayerPrefs.GetString("2S[" + No + "]");
            }
            if (PlayerPrefs.HasKey("T^2[" + No + "]"))
            {
                go.transform.Find("T^2").GetComponent<InputField>().text = PlayerPrefs.GetString("T^2[" + No + "]");
            }
            if (PlayerPrefs.HasKey("a[" + No + "]"))
            {
                go.transform.Find("a").GetComponent<InputField>().text = PlayerPrefs.GetString("a[" + No + "]");
            }
            if (PlayerPrefs.HasKey("g[" + No + "]"))
            {
                go.transform.Find("g").GetComponent<InputField>().text = PlayerPrefs.GetString("g[" + No + "]");
            }
        }
    }

    public bool mayShowGraph(){
        int countOfFilled = 0;
        for(int i=0; i<No; i++){
            if(this.transform.Find("DR_" + i).GetComponent<IsFilled>().isFilled()){
                countOfFilled++;
            }
        }
        if(countOfFilled>=2){
            return true;
        }
        else{
            return false;
        }
    }

    public void toCSV(){
        Debug.Log("started to CSV");
        log.gameObject.SetActive(true);
        log.text += "\nstarting";
        string collector = string.Empty;
        int localNo = 0;
        
        for (int i = 0; i < PlayerPrefs.GetInt("No"); i++)
        {
            // session_id,write_time,obs_no,theta,s,t1,t2,T,2S,t^2,A,G
            localNo = i + 1;
            collector += "\n" + PlayerPrefs.GetString("SessionID") + "," + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "," + localNo.ToString();
            if(PlayerPrefs.HasKey("FinalAngle")){
                collector += "," + PlayerPrefs.GetString("FinalAngle");
            }
            else{
                collector += ",";
            }
            if(PlayerPrefs.HasKey("S[" + localNo + "]")){
                collector += "," + PlayerPrefs.GetString("S[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if(PlayerPrefs.HasKey("T1[" + localNo + "]")){
                collector += "," + PlayerPrefs.GetString("T1[" + localNo + "]");
            }                
            else{
                collector += ",";
            }
            if(PlayerPrefs.HasKey("T2[" + localNo + "]")){
                collector += "," + PlayerPrefs.GetString("T2[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if (PlayerPrefs.HasKey("T[" + localNo + "]"))
            {
                collector += "," + PlayerPrefs.GetString("T[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if (PlayerPrefs.HasKey("2S[" + localNo + "]"))
            {
                collector += "," + PlayerPrefs.GetString("2S[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if (PlayerPrefs.HasKey("T^2[" + localNo + "]"))
            {
                collector += "," + PlayerPrefs.GetString("T^2[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if (PlayerPrefs.HasKey("a[" + localNo + "]"))
            {
                collector += "," + PlayerPrefs.GetString("a[" + localNo + "]");
            }
            else{
                collector += ",";
            }
            if (PlayerPrefs.HasKey("g[" + localNo + "]"))
            {
                collector += "," + PlayerPrefs.GetString("g[" + localNo + "]");
            }
            else{
                collector += ",";
            }
        }

        log.text += " | about to write " + localNo + " obs";
        string data = hdrCSV + collector;
        string path = Path.Combine(GetPersistanceLocation(), fNameCSV);
        log.text += " to " + path;
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(data);
        writer.Close();

        result.text += "\nOK";
    }

    private string GetPersistanceLocation(){
        string folderPath = strResPathObs;
        if(Application.isEditor){
            // working in Unity Editor
            folderPath = Path.Combine(Path.Combine("Assets", "Resources"), folderPath);
        }
        else{
            // working on deployment target
            folderPath = Path.Combine(Application.persistentDataPath, folderPath);
            if(!Directory.Exists(folderPath)){
                Debug.Log("going to create new folder for table of observations at " + folderPath);
                Directory.CreateDirectory(folderPath);
            }
        }
        return folderPath;
    }

    public void dummmy(){
        Debug.Log("dummy");
    }

}
